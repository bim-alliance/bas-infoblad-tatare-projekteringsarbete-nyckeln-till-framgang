![Bild 1](media/1.jpeg)

**Målet med projekteringsmodellerna vid ett ombyggnadsprojekt i Lund var att de skulle utgöra en grund för teknikkontroll och kontinuerligt kunna användas på olika projekteringsmöten.** 

# Tätare projekteringsarbete nyckeln till framgång

> ##### Det är viktigt att se projekteringsmodeller som arbetsredskap och nulägesbeskrivningar och inte som färdiga modeller som är ”snygga” att titta på. Idag utgör tekniken inget hinder för BIM. Det är förhållningssättet och attityden till BIM som behöver utvecklas och bli bättre, anser Pontus Bengtson, ansvarig BIM transport och infrastruktur inom WSP Group.

UNDER HÖSTEN HAR EN KORSNING I VÄSTRA Lund byggts om till en cirkulationsplats för att underlätta åtkomst till ett handelscentrum. Här har även anlagts ny gång- och cykelväg under väg 16. BIM har använts i projekteringen av detta normalstora infrastrukturprojekt.
​	– I stora projekt är det så gott som alltid möjligt att testa något nytt och man kan ofta se effekter direkt i projektet. Men vi bör även ta in processtänkandet i mindre projekt och därför gjorde vi inom WSP, trots att Trafikverket inte ställde några krav
på BIM, ombyggnaden i Lund till ett utvecklingsprojekt, berättar Pontus Bengtson.
​	Grundtanken var att hela tiden arbeta med kontinuerligt uppdaterade projekteringsmodeller. Samtliga aktuella teknikområden – väg, VA, belysning, landskap, bro och konstruktion – fanns med i modellerna som, under intensiva perioder, uppdaterades flera gånger om dagen.
​	Målet med modellerna var att de skulle utgöra en grund för teknikkontroll och kontinuerligt kunna användas på olika projekteringsmöten. Pontus Bengtson poängterar att det inte fick vara något ”handpåläggande” för att modellerna skulle se ”snygga” ut.
​	– Vi inom infrastruktursektorn är så fixerade vid att det inte får finnas några hål i de modeller vi presenterar. Det anses inte se bra ut och kan tyda på att man inte klarar av att göra en fullständig modell. Men det spelar ingen roll när det handlar om teknik och rena projekteringsmöten. Ändå kan en del inte bortse från att det måste se snyggt ut. Som vanligt när det är något nytt är många osäkra och rädda att modellen används i ett annat sammanhang än det avsedda.
​	Egentligen, menar Pontus Bengtson, skulle det vara bättre att tala om VE, virtual engineering, istället för VR, virtual reality. Då betonas att det i första hand handlar om tekniken vilket ger större acceptans för modellens visuella brister.
​	Det är mycket viktigt att vid projektstarten tänka strategiskt och svara på frågan: Varför väljer vi att arbeta så här? Om alla kan enas om ett svar kan man släppa på prestigen och se varje uppdaterad modell som en nulägesbeskrivning av hur projekteringen ser ut just nu och inte som något som ska presenteras för politiker och allmänhet. Det handlar om att få till stånd en attitydförändring.
​	Vid det första ledningsägarmötet i lundaprojektet visades en modell där alla befintliga kablar och ledningar lagts in. Erfarenhetsmässigt händer inte mycket under det första mötet men en konkret modell att studera innebar en helt annan aktivitet än vad som normalt äger rum. Modeller ger möten ett annat fokus jämfört med när man bara har ritningar och dessutom förstår alla var man befinner sig och vad man pratar om.

![Bild 2](media/2.jpeg)

**Egentligen skulle det vara bättre att tala om VE, virtual engineering,**
**än om VR, virtual reality, menar Pontus Bengtson.**

Summeringen av BIM i lundaprojektet är att teknikområdena hade klart bättre förståelse för varandra och bättre träffbild på vad man gjorde. Det blev färre krockar och det som levererades till entreprenören för maskinstyrning hade mycket hög
säkerhet. Möten med beställare och andra aktörer blev både roligare och effektivare; engagemang och förståelse blir större när man tydligt i modellen ser hur arbetet fortskrider.
​	– Skillnaden jämfört med vanlig projektering är stor. BIM gör det möjligt att ta tillbaka hantverket. I ett projekt förändrar vi hela tiden förutsättningarna för varandra. Här har vi en process och en metod som gör det möjligt att kontinuerligt fånga
upp detta, vilket innebär att vi, jämfört med tidigare, samlar ihop oss tätare i projekten. Detta är nyckeln till framgång.

VARFÖR ANVÄNDS DÅ INTE BIM och modeller oftare? Enligt Pontus Bengtson beror det just på att man är rädd för att visa icke färdigtänkta saker. Men vem har ansvar när man visar något? Den som visar eller den som tittar?
​	– Generellt missar vi att det är den som tittar som har ansvar för vad som ska hända nästa gång. Hur agerar du? Hur reagerar du? Man kan inte se en modell som en färdig bygghandling och reagera utifrån det. Hela processen bygger på att visa vad man tänker – inte vad man tänkt – för att därmed få till stånd en diskussion. Strul i dagens projektering beror på att vi håller lite för länge på informationen, eftersom vi inte vill att någon ska tro att vi inte kan. Det handlar mycket om respekt och förtroende. Att utveckla BIM-processen handlar visserligen om teknik och verktyg men än mer om mänskliga egenskaper och attityder, säger Pontus Bengtson.
​	Han menar att det i dag läggs väldigt mycket tid på att prata teknik och verktyg men alltför lite tid på att diskutera hur man ska förhålla sig till varandra i projektet och hur man bäst ska hantera saker och ting.

GÅR DET SNABBARE ATT GENOMFÖRA ett projekt med hjälp av BIM? Omöjligt att värdera, enligt Pontus Bengtson. Det kanske inte går snabbare men BIM ger en
helt annan träffbild och kvalitet och leder till att man fångar upp fler frågeställningar än man gör i ett traditionellt projekt. Det är exempelvis värdelöst att upptäcka en telebrunn i byggskedet men svårt att värdera vad detta kostar i pengar.
​	– Nästan alla är överens om att BIM är bra. Vi borde kunna komma överens om att se BIM på lång sikt och om fem år se om det skett en produktivitetsökning. Det gäller att tro på värdet med BIM – idag läggs alltför mycket kraft på att försöka mäta effekterna av BIM i varje pilotprojekt, säger Pontus Bengtson och fortsätter:
​	– Det som behöver utvecklas och bli bättre är förhållningssättet och attityden till BIM. Tekniken utgör inte den huvudsakliga begränsningen idag – vi kan göra mycket redan nu bara vi kan få in i våra huvuden att det är bättre att nå åttio än noll procent. Vi kan inte utveckla allt på en gång utan det måste ske steg för steg. Och vi måste ha mättal som är baserade på mer än ett eller en knippe av projekt och bara ett år.
Vi kommer aldrig att få en produktivitetsutveckling genom att byta verktyg. Om det ska bli en förändring, som ger en produktivitetsutveckling, så behöver vi förändra vårt arbetssätt och bli mer processorienterade. Vi bör fokusera på mjuka delar
till åttio procent och lägga tjugo på verktyg.

